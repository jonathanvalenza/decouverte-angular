import { Component, OnInit } from '@angular/core';
import { ListFormationService } from 'src/app/services/list-formation.service';

@Component({
  selector: 'app-list-formation',
  templateUrl: './list-formation.component.html',
  styleUrls: ['./list-formation.component.css']
})
export class ListFormationComponent implements OnInit {

  public formations; // propriété
  constructor(private listFormationService: ListFormationService) { }

  ngOnInit() {
    this.listFormationService.getFormation().subscribe((results) => {
      this.formations = results;
    });
  }
}

