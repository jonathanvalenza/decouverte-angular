import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListFormationDetailComponent } from './list-formation-detail.component';

describe('ListFormationDetailComponent', () => {
  let component: ListFormationDetailComponent;
  let fixture: ComponentFixture<ListFormationDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListFormationDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListFormationDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
