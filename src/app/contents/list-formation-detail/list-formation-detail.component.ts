import { Component, OnInit } from '@angular/core';
import { ListFormationService } from 'src/app/services/list-formation.service';

@Component({
  selector: 'app-list-formation-detail',
  templateUrl: './list-formation-detail.component.html',
  styleUrls: ['./list-formation-detail.component.css']
})
export class ListFormationDetailComponent implements OnInit {
public formationDetail;
  constructor(private listFormationService: ListFormationService) { }

  ngOnInit() {
    this.listFormationService.getFormationDetail().subscribe((results) => {
      this.formationDetail = results;
    });
  }
}
