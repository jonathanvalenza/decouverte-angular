import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/app/account.service';

@Component({
  selector: 'app-list-account',
  templateUrl: './list-account.component.html',
  styleUrls: ['./list-account.component.css']
})
export class ListAccountComponent implements OnInit {

  public accounts; // propriété
  constructor(private accountService: AccountService) { }

  ngOnInit() {
    // ici on demande les accounts qui alimentent le constructeur au dessus.
    // subscribe est une promesse (équivalent du sucess de jquery)
    this.accountService.getAccounts().subscribe((results) => {
      this.accounts = results;
    });
  }
}
