import { Component, OnInit } from '@angular/core';
import { ListFormationService } from 'src/app/services/list-formation.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail-formation',
  templateUrl: './detail-formation.component.html',
  styleUrls: ['./detail-formation.component.css']
})
export class DetailFormationComponent implements OnInit {
public detailFormation;
public id;
public students;
public modules;
  constructor(private listFormationService: ListFormationService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.listFormationService.getFormationDetailById(this.id).subscribe((results) => {
      this.detailFormation = results[0];
    });
    this.id = this.route.snapshot.paramMap.get('id');
    this.listFormationService.getStudentByFormation(this.id).subscribe((results) => {
      this.students = results;

    });
    this.id = this.route.snapshot.paramMap.get('id');
    this.listFormationService.getModuleByFormation(this.id).subscribe((results) => {
      this.modules = results;

    });
  }
}
