import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListAccountComponent } from './contents/list-account/list-account.component';
import { ListFormationComponent } from './contents/list-formation/list-formation.component';
import { ListFormationDetailComponent } from './contents/list-formation-detail/list-formation-detail.component';
import { DetailFormationComponent } from './contents/detail-formation/detail-formation.component';

const routes: Routes = [
  {path: 'listAccount', component : ListAccountComponent},
  {path: 'listFormation', component : ListFormationComponent},
  {path: 'listFormationDetail', component : ListFormationDetailComponent},
  {path: 'detailFormation/:id', component : DetailFormationComponent}
];

@NgModule({
  imports : [RouterModule.forRoot(routes)],
  exports : [RouterModule]
})
export class RoutingModule { }
