import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ListFormationService {

  constructor(private http: HttpClient) { }

  getFormation() {

    return this.http.get('http://localhost:3000/formation');

  }

  getFormationDetailById(id) {

    return this.http.get('http://localhost:3000/formationDetailById/' + id);

  }

  getStudentByFormation(id) {

    return this.http.get('http://localhost:3000/studentByFormation/' + id);

  }

  getModuleByFormation(id) {

    return this.http.get('http://localhost:3000/moduleByFormation/' + id);

  }

  getFormationDetail() {

    return this.http.get('http://localhost:3000/formationDetail');

  }
}
