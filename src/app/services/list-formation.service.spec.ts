import { TestBed } from '@angular/core/testing';

import { ListFormationService } from './list-formation.service';

describe('ListFormationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListFormationService = TestBed.get(ListFormationService);
    expect(service).toBeTruthy();
  });
});
