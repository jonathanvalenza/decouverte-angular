import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TopbarComponent } from './topbar/topbar.component';
import { NavigationComponent } from './navigation/navigation.component';
import { EvenementsComponent } from './evenements/evenements.component';
import { ListAccountComponent } from './contents/list-account/list-account.component';
import { AccountService } from './account.service';
import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { RoutingModule } from './routing.module';
import { RouterModule } from '@angular/router';
import { ListFormationComponent } from './contents/list-formation/list-formation.component';
import { ListFormationService } from './services/list-formation.service';
import { ListFormationDetailComponent } from './contents/list-formation-detail/list-formation-detail.component';
import { DetailFormationComponent } from './contents/detail-formation/detail-formation.component';

@NgModule({
  declarations: [
    AppComponent,
    TopbarComponent,
    NavigationComponent,
    EvenementsComponent,
    ListAccountComponent,
    ListFormationComponent,
    ListFormationDetailComponent,
    DetailFormationComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule, // import du module http qui comprend le http service que je renseigne dans le providers
    RoutingModule,
    RouterModule
  ],
  providers: [
    HttpClient,
    AccountService,
    ListFormationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
