# Angular decouverte - partie 1 les composant
## Documentation
https://angular.io/

base de travail
http://angular.io/tutorial

### Présentation d'angular
Angular framework de developpement front end. Il permet de créer en programmation objet de l'applicatif front end à destination des navigrateurs. Le developpement de se fait en TypeScript est transpillé (build) en javascript afin de pouvoir etre déployé en production
La struture d'un application sous angular est composé de "components"
Un "component" est composé d'une interface utilisateur et d'un applicatif aussi nommé controleur. Un component est donc composé d'un fichier de vue plus d'une classe associée à cette vue.

Une interface utilisateur est composée de plusieurs "components"

### Syntaxe et règles du language
- Toujours importer les classes dont nous avons besoin.

### Tour d'horizon de la struture des dossiers
- package.json : fichier qui liste des dépendances (voir npm)
- angular.json : le fichier de configuration de l'application
- src/index.html : le point d'enter de l'application
- src/assets/ : dossier des fichiers externe (bibliothèques, images, fonts..)
- src/environements/ : les configurations des différents environnements.
- src/app/ : le composant de base de l'application
   - src/app/app.module.ts : lister tous les modules, services et composants de l'application.

### Etude du composant de base "AppComponent"
- src/app/app.component.ts : le contrôleur de AppComponent.
Ce fichier est un classe TypeScript.Au dessus de la classe est ecrit le "parametrage" de cette classe.
En typeScript, comme pour tous les langages de programmation la liste de imports est ecrite au debut du fichier. (comme les Uses pour les NameSpaces).
Pour un "Component", il faut importer la classe avec un objet du type "Component".
L'annotation va nous permetttre de parametrer le composant.
- templatURL : permet de lier le controller avec la vue
- selector : permet de definir le nom de la balise pour l'include.

### Génération de composant en CLI ( commande line interface)
`ng generate component nomducomposant`
ou
`ng generate component adresse/nomducomposant`

### Les composants

Ajouter le composant dans src/app/app.module.ts dans la liste des déclarations

https://angular.io/api/core/component

Un composant est une brique d'une application angular. Une application est donc composée de plusieurs briques.
Chaque brique est composé d'une interface utilisateur et de son controleur associé.
Le controleur sert d'interface entre lui et le reste de l'application (backend).
Par exemple, on peux lier les propriétés des controleurs avec des zones dans l'interface.
Un composant peut être composé de composants. (ex : app-root est le composant principal et est composé de composants "metier").
Lorsque noous avons besoin d'un objet particulié au sein du composant, avec angular, nous n'instancion pas directement l'objet. Nous pratiquons ce que l'on appele l'injection de dépendance : 
Nous surchargeons le constructer afin qu'il prenne en argument les objets dont nous avons besoin :
`construct(private router: Routeur) {}`
Pas besoin de définir la propriété dans la classe de manière classique, ici angular "sait" que la propriété router va être utiliser dans la classe.
Le contructeur est surchargé pour prendre un objet de type router en argument, Angular va automatiquement passer un objet de ce type à la construction de notre composant.

L'annotation "@component" va permetre de parametrer notre classe. Les essentiels sont les suivants :
- Le selector qui va permettre de définir l'endroit ou va se positionner la vue de notre composant.
- Le chemin du template, qui contient le code html du compoant et le chemin du fichier css.

par exemple : <nom-de-la-balise></nom-de-la-balise>
```ts
@component({
   selector: 'nom-de-la-bailse',
   templateURL: './template-html.html',
   styleURLs:['./style-css.css']
})
```

Il est possible de  pouvoir directement ecrire du code dans les valeurs de templateUrl et styleUrl.
```ts
@component({
   selector: 'nom-de-la-bailse',
   templateURL: '<p>Bonjour à tous</p>',
   styleURLs:['p{background-color:red;}']
})
```
Lors de la génération du composant, Angular lui fait implémenter l'interface "onInit", ce qui nous impose d'implémenter la methode ngOnInit();
Cette methode permet d'initialiser l'état de l'objet
- le constructeur ne sert exclusivement qu'à l'injection de dépendence
- l'initialisation se fait dans la methode ngOnInt()

Tous comme il est possible de lier une propriété a une zone de l'interface utilisateur, il est possible de lier des action a certaine zone en capturant un evenement particulié (onclick, on event...). 
```html
<a>(click)="go()"</a>
```

La methode go() sera invoquée lorsque l'évenement onClick sera capturé sur la balise <a>.

La liste des directive utilisable dans la vue sont disponible ici :
https://angular.io/api?type=directive

Gestion des evenements :

https://angular.io/guide/template-syntaxe#event-binding

### Le routing

C'est un module spécifique basé sur le router module d'Angular.
l'annotation montre que nous customisons le routingModule d'Angular
```ts
const ROUTES:routes = [
    {path: 'chemin', component : NomDuComposantComponent}
];
@NgModule({
  imports : [RouterModule.forRoot(ROUTES)],
  exports : [RouterModule]
})
```

Ici, on importe dans notre module le RouterModule dans lequel on rajoute nos routes et on le ré-exporte.
Dans les routes, on définie quel composant sera chargé sur quel route. La zone dans lequel laquelle sera chargé le composant ciblé et délimité par :
```html
<router-outlet></router-outlet>
```

l'évènement qui permetra le changement sera défini grace à la directive "routerLink" sur la vue :
```html
<a routerLink="/path-du-component">lien vers le component </a>
```


### Les modules

Ajouter le module dans src/app/app.module.ts dans la liste des imports

C'est une bibliotheque composé de modules, composant, de services.

### les services

Ajouter le service dans src/app/app.service.ts dans la liste des providers

Les services sont l'équivalent du DAO dans la backend. Du point de vue du controleur, interroger un service, revient à invoquer une methode du DAO dans le cas d'un backend. 
Ici, le therme service implique la notion d'asynchrone, de tache de fond. En outre, les services sont des objets "injectable" :

```ts
@injectable({
   provideIn: "root"
})
```

Les services seront donc "injecter" dans les constructeurs des composants.
https://angular.io/api/core/Injectable


======
pense bete
======
ordre d'utilisation pour afficher des valeur dans la vue :

1 - main.js pour la requete sql
2 - dans le service on va chercher les info de main.js
3 - dans le component, on va chercher la methode du service (avec import et tous ça tous ça) et on rentre le resultat dans l'attribut
4 - dans la vue on utilise l'attribut du component

Création de component :

1 - créeér le component avec ng generate component
2 - (si on utilise plusieurs services, créer un service)
3 - ajouter le path dans la routing
4 - ajouter dans app.module.ts dans @NgModule({ declarations:

creation de class :

1 - créer la class avec ng generate class adresse/nomDeClass
2 - remplir les attributs de la classe
